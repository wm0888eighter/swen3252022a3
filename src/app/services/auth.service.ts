import { Injectable } from '@angular/core';
import {
	Auth,
	signInWithEmailAndPassword,
	createUserWithEmailAndPassword,
	signOut,
    user
} from '@angular/fire/auth';
import { BehaviorSubject, from, Observable, Subject } from 'rxjs';

@Injectable({
	providedIn: 'root'
})
export class AuthService {
	constructor(private auth: Auth) {}
    isAuthenticated: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(null);
	async register({ userName, email, password }) {
		try {
			const user = await createUserWithEmailAndPassword(this.auth, email, password);
            this.isAuthenticated.next(true);
            return user;
		} catch (e) {
            this.isAuthenticated.next(false);
			return null;
		}
	}

	async login({ email, password }) {
		try {
			const user = await signInWithEmailAndPassword(this.auth, email, password);
            this.isAuthenticated.next(true);
            return user;
		} catch (e) {
            this.isAuthenticated.next(false);
			return null;
		}
	}

	logout() {
        this.isAuthenticated.next(false);
		return signOut(this.auth);
	}

    getUserName(){
        return user(this.auth);
    }
}