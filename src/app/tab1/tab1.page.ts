import { Component, ViewChild, ElementRef } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/compat/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AlertController } from '@ionic/angular';

import { Geolocation } from '@capacitor/geolocation';


@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

 // Firebase Data
 locations: Observable<any>;
 locationsCollection: AngularFirestoreCollection<any>;

 // Location Tracking
 isTracking = false;
 watch: string;
 user = null;
 

 constructor(
  private afAuth: AngularFireAuth, 
  private afs: AngularFirestore, 
  private alertController: AlertController,) {
   this.onLogin();
 }

 

 // Load data to the current user
 onLogin() {
  this.afAuth.currentUser.then(res => {
    this.user = res;

    this.locationsCollection = this.afs.collection(
      `locations/${this.user.uid}/track`,
      ref => ref.orderBy('timestamp')
    );

    // Make sure we also get the Firebase item ID!
    this.locations = this.locationsCollection.snapshotChanges().pipe(
      map(actions =>
        actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        })
      )
    );
    
  });
}
 
startTracking() {
  this.isTracking = true;
  
  let w = Geolocation.watchPosition({}, (position, err) => {
    if (position) {
      this.addNewLocation(
        position.coords.latitude,
        position.coords.longitude,
        position.timestamp
      );
      
    }
  });
  w.then(res => {
    this.watch = res;
  })
}

// Unsubscribe from the geolocation watch using the initial ID
stopTracking() {
  Geolocation.clearWatch({ id: this.watch }).then(() => {
    this.isTracking = false;
  });
}

// Save a new location to Firebase and center the map
addNewLocation(lat, lng, timestamp) {
  this.locationsCollection.add({
    lat,
    lng,
    timestamp
  });

  
}

// Delete a location from Firebase
deleteLocation(pos) {
  this.locationsCollection.doc(pos.id).delete();
}

// Present alert for delete a location
async presentAlert(pos) {
  const alert = await this.alertController.create({
    header: 'Are you sure delete this location?',
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        
      },
      {
        text: 'Yes',
        role: 'confirm',
        handler: () => {
          this.deleteLocation(pos) ;
        },
      },
    ],
  });

  await alert.present();  
}
}
