import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { Firestore, collectionData, collection } from '@angular/fire/firestore';
import { WeatherService } from '../../services/weather.service';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-forecast',
  templateUrl: './forecast.html',
  styleUrls: ['./forecast.scss']
})
export class Forecast {
    locations: Observable<any>;
    currentWeather: any = <any>{};
    user = null;
    forecast: any = <any>{};
    msg: string;
  constructor(
    private afAuth: AngularFireAuth, 
    private firestore: Firestore,
    private weatherService: WeatherService,
    private alertController: AlertController,
  ) {
    this.afAuth.currentUser.then(res => {
        this.user = res;
    
        const col = collection(firestore, `locations/${this.user.uid}/track`);
        this.locations = collectionData(col);   
        
      });
  }
  ngOnInit() {
  }
  pos;
  searchWeather() {
    this.msg = '';
    this.currentWeather = {};
    this.weatherService.getCurrentWeather(this.pos.lat, this.pos.lng)
      .subscribe(res => {
        this.currentWeather = res;
      }, err => {
        if (err.error && err.error.message) {
          this.msg = err.error.message;
          this.presentAlert(this.msg)
          return;
        }
        this.presentAlert('Failed to get weather.');
      }, () => {
        this.searchForecast(this.pos.lat, this.pos.lng);
})
  }
  searchForecast(lat, lng) {
    this.weatherService.getForecast(lat, lng)
      .subscribe(res => {
        this.forecast = res;
      }, err => {
})
  }

  resultFound() {
    return Object.keys(this.currentWeather).length > 0;
  }

  async presentAlert(msg) {
    const alert = await this.alertController.create({
      header: 'Error',
      message: msg,
      buttons: ['OK'],
    });
  
    await alert.present();  
  }
}