import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Tab2Page } from './tab2.page';

const routes: Routes = [
  {
    path: '',
    component: Tab2Page,
    children: [
      {
        path: 'forecast',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./forecast/forecast.module').then(m => m.forecastModule)
          }
        ]
      },
      {
        path: 'current',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./current/current.module').then(m => m.currentModule)
          }
        ]
      },
      {
        path: 'uv',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./uv/uv.module').then(m => m.uvModule)
          }
        ]
      },
      {
        path: '',
        redirectTo: 'current',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tab2',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Tab2PageRoutingModule {}
