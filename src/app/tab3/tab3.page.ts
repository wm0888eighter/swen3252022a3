import { AuthService } from '../services/auth.service';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/compat/auth';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})

export class Tab3Page {
  user = null;

  constructor(private authService: AuthService, private router: Router, private afAuth: AngularFireAuth) {
    this.afAuth.currentUser.then(res => {
      this.user = res;
  });
}
  // logout for user
	async logout() {
		await this.authService.logout();
		this.router.navigateByUrl('', { replaceUrl: true });
	}
}