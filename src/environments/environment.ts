// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiKey: 'f88afc81cbbe53adb0b2b1c62a0371ef',
  apiUrl: 'http://api.openweathermap.org/data/2.5',
  firebase: {
    apiKey: "AIzaSyDa2-jj_GXsIZeF2uObhFs8oa_J6Lihllg",
    authDomain: "livelocation-364114.firebaseapp.com",
    projectId: "livelocation-364114",
    storageBucket: "livelocation-364114.appspot.com",
    messagingSenderId: "186437290089",
    appId: "1:186437290089:web:48c241ba5d5c8d1c607af8",
    measurementId: "G-EKSBNYMSBS"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
