# Location Tracking with weather search by "Build Your First Ionic App: Photo Gallery (Ionic Angular and Capacitor)" template

Powered by [Ionic Angular](https://ionicframework.com/docs/angular/overview) (web app) and [Capacitor](https://capacitor.ionicframework.com) (native app runtime) .

## How It Works
After the user registed or login then navigates to Tab 2 (Weather), they can tap/click on the current tab or forecast tab or uv tab to get the current weather or weather forecast or uv data with a selected location. 

After the user navigates to Tab 1 (Location Tracking), they can tap/click on the start Tracking button to start the location tracking by geolocation. They also can tap/click on the stop Tracking button to stop the location tracking. And they can slide right on the selected location then press the trashs icon to delete the location.

After the user  navigates to Tab 3 (my Account), they can tap/click on the logout button to logout.

## Feature Overview
* App framework: [Angular](https://angular.io)
* UI components: [Ionic Framework](https://ionicframework.com/docs/components)
* Native runtime: [Capacitor](https://capacitor.ionicframework.com)
  * Location Tracking: [Geolocation API](https://capacitorjs.com/docs/apis/geolocation)
  * Storing Location to the firebase: [AngularFirestore](https://github.com/angular/angularfire)
  * Storing users: [AngularFireAuth](https://github.com/angular/angularfire)
  * Weater Information: [openWeathermap API](https://openweathermap.org/api)

## Project Structure
* Login page (`src/app/login/`): Login UI and basic logic.
* AuthService (`src/app/services/auth.service.ts`): Logic encapsulating AngularFireAuth.

* Tab1 (Location Tracking) (`src/app/tab1/`): Location Tracking UI and basic logic.

* Tab2 (Weather) (`src/app/tab2/`): Weather information UI and basic logic.
* Current (`src/app/tab2/current`): Current weather information UI and basic logic.
* Forecast (`src/app/tab2/forecast`): Weather forecast information UI and basic logic.
* Uv (`src/app/tab2/uv`): Uv data UI and basic logic.
* WeatherService (`src/app/services/weather.service.ts`): Logic encapsulating opewnweathermap APIs.

* Tab3 (My Account) (`src/app/tab3/`): Display current user email and logout button.
* AuthService (`src/app/services/auth.service.ts`): Logic encapsulating AngularFireAuth.
